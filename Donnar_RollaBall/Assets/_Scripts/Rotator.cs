﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    // Update is called once per frame
    //allows to edit in unity.
    public float speed;
    public float forceMult = 200;
    private Rigidbody rb;

    Vector3 dir;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();


        ChooseDirection();
    }
    
    void FixedUpdate()
    {

        /*
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);

        // Attempting to make the collectibles move forward but not go through walls.
        Vector3 newPosition = transform.position + (transform.forward * Time.deltaTime);
        rb.MovePosition(newPosition);
        */

        rb.velocity += dir * speed * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        ChooseDirection();

    }
    // Chooses a random direction within a circle for the pick up to go to.
    void ChooseDirection()
    {
        Vector2 randomdir = Random.insideUnitCircle;
        dir = new Vector3(randomdir.x, 0f, randomdir.y);
    }
}

